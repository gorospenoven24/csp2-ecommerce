const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");


// add product by admin only
router.post("/products", auth.verify, (req, res) => {
			const data = auth.decode(req.headers.authorization)
			productController.addProduct(data, req.body).then(resultFromController => res.send(resultFromController));

		});


// route  for retrieving all active product
router.get("/products",(req,res)=>{
	productController.getAllActive().then(resultFromController => res.send(resultFromController	));
})


// route for retriver a specific product
router.get("/:productId", (req,res)=>{
	console.log(req.params.productId);
	productController.getProduct(req.params).then(resultFromController =>res.send(resultFromController));
})


//  route for updating product by admin only
router.put("/:productId", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)
	productController.updateProduct(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
})


//  route for archiving product
router.put("/:productId/archive", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)
	productController.archivedProduct(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
})


module.exports = router
