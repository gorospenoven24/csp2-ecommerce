const mongoose = require("mongoose");

//  Creat euser with the following properties
const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "Email is required"]
	},
	lastName : {
		type : String,
		required : [true, "Email is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	orders:[{
		orderId:{
			type: String
		}
		
	}]



});
	
module.exports = mongoose.model("User", userSchema);
