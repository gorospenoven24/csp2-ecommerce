const Product = require("../models/Product");
const userController = require("../controllers/user");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.addProduct = async (user,reqBody, res)=>{
	if(user.isAdmin){
		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})
		return newProduct.save().then((product,error)=>{
		if (error) {
			return false;
		}else{
			
			return	true;
		}

	})

	}
	else{
		return("you have no access")
	}
}

// Retrieve all active product
 module.exports.getAllActive =() =>{
 	return Product.find({isActive : true}).then(result =>{
 		return result;
 	})
 }

 // Retrieving a secific product
 module.exports.getProduct = (reqParams)=>{
 	return Product.findById(reqParams.productId).then(result =>{
 		return result;
 	})
 }

// update product by admin only
  module.exports.updateProduct = async (user,reqParams,reqBody)=>{
	 	if(user.isAdmin){
	 		 	// specify the fileds/properties of the document to be updated
	 	let updatedProduct ={
	 		name :reqBody.name,
	 		description: reqBody.description,
	 		price: reqBody.price,
	 		isActive:reqBody.isActive
	 	}
	 	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then ((product,error)=>{
	 		if(error){
	 			return false;
	 		}
	 		else{
	 			return true;
	 		}
	 	})
 	}
 	else{
 			return("you have no access")
 	}


 }

// archived a Product
 module.exports.archivedProduct = async (user,reqParams,reqBody)=>{
	 	if(user.isAdmin){
	 		 	// specify the fileds/properties of the document to be updated
	 	let archivedProduct ={
	 		isActive :reqBody.isActive
	 		
	 	}
	 	return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then ((product,error)=>{
	 		if(error){
	 			return false;
	 		}
	 		else{
	 			return true;
	 		}
	 	})
 	}
 	else{
 			return("you have no access")
 	}


 }


 // //////////////////////////////////
 // activty
