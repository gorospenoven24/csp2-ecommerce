const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
//const orderRoutes = require("./routes/order");


const app = express();

// Connect to MongoDB
mongoose.connect("mongodb+srv://dbnovengorospe:efBaWxyisxGMqFY8@wdc028-course-booking.68ll3.mongodb.net/ecommerceDB?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology : true
	}

);
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Defines the "users" string to be included for all user routes defined in the "user" route file
 app.use("/users", userRoutes);

 app.use("/products", productRoutes);


// Listening to port
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});
